package sample;

import org.junit.Test;
import sample.Function;
import org.junit.Assert;

import static org.junit.Assert.*;

public class FunctionTest {

    @Test
    public void lenthConvert() {
        String convertFrom = "m";
        String convertTo = "km";
        Double firstNum = 5.0;
        Double secondNum = 5.0;
        Boolean firstNumToSecond = true;
        String expected = "0,005";
        String actual = Function.lenthConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);
        assertEquals(expected, actual);
    }

    @Test
    public void temperatureConvert() {
        String convertFrom = "C";
        String convertTo = "N (Шкала Ньютона)";
        Double firstNum = 1.0;
        Double secondNum = 1.0;
        Boolean firstNumToSecond = true;
        String expected = "3,03";
        String actual = Function.temperatureConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);
        assertEquals(expected, actual);
    }

    @Test
    public void weightConvert() {
        String convertFrom = "kg";
        String convertTo = "c";
        Double firstNum = 2.0;
        Double secondNum = 2.0;
        Boolean firstNumToSecond = true;
        String expected = "200";
        String actual = Function.weightConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);
        assertEquals(expected, actual);
    }

    @Test
    public void timeConvert() {
        String convertFrom = "sec";
        String convertTo = "Min";
        Double firstNum = 60.0;
        Double secondNum = 60.0;
        Boolean firstNumToSecond = true;
        String expected = "1";
        String actual = Function.timeConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);
        assertEquals(expected, actual);
    }

    @Test
    public void volumeConvert() {
        String convertFrom = "l";
        String convertTo = "m^3";
        Double firstNum = 1000.0;
        Double secondNum = 1000.0;
        Boolean firstNumToSecond = true;
        String expected = "1";
        String actual = Function.volumeConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);
        assertEquals(expected, actual);
    }
}