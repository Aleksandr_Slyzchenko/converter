package sample;

import java.text.DecimalFormat;

public class Function {

    static DecimalFormat df = new DecimalFormat("###.###");

    public static String lenthConvert(String from, String to, Double first, Double secod, Boolean firstNumToSecond) {

        switch (to) {
            case "km":
                if (firstNumToSecond)
                    return String.valueOf(df.format(first / 1000));
                else
                    return String.valueOf(df.format(secod * 1000));
            case "mile":
                if (firstNumToSecond)
                    return String.valueOf(df.format(first / 1609.34));
                else
                    return String.valueOf(df.format(secod * 1609.34));
            case "nautical mile":
                if (firstNumToSecond)
                    return String.valueOf(df.format(first / 1852));
                else
                    return String.valueOf(df.format(secod * 1852));
            case "cable":
                if (firstNumToSecond)
                    return String.valueOf(df.format(first / 185.2));
                else
                    return String.valueOf(df.format(secod * 185.2));
            case "league":
                if (firstNumToSecond)
                    return String.valueOf(df.format(first / 5556));
                else
                    return String.valueOf(df.format(secod * 5556));
            case "foot":
                if (firstNumToSecond)
                    return String.valueOf(df.format(first / 0.3048));
                else
                    return String.valueOf(df.format(secod * 0.3048));
            case "yard":
                if (firstNumToSecond)
                    return String.valueOf(df.format(first / 0.9144));
                else
                    return String.valueOf(df.format(secod * 0.9144));
            default: return "Идентичные величины конвертации!";
        }
    }

    public static String temperatureConvert(String convertFrom, String convertTo, Double firstNum, Double secondNum, Boolean firstNumToSecond) {

        switch (convertTo) {
            case "K (Шкала Кельвина)":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum + 273.15));
                else
                    return String.valueOf(df.format(secondNum - 273.15));
            case "Re (Шкала Реомюра)":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum * 1.25));
                else
                    return String.valueOf(df.format(secondNum / 1.25));
            case "F (Шкала Фаренгейта)":
                if (firstNumToSecond)
                    return String.valueOf(df.format((firstNum*1.8)+32));
                else
                    return String.valueOf(df.format((secondNum-32)/1.8));
            case "Ro (Шкала Рёмер)":
                if (firstNumToSecond)
                    return String.valueOf(df.format((firstNum*21/40)+7.5));
                else
                    return String.valueOf(df.format((secondNum-7.5)*40/21));
            case "Ra (Шкала Ранкина)":
                if (firstNumToSecond)
                    return String.valueOf(df.format((firstNum*1.8)+32+459.67));
                else
                    return String.valueOf(df.format((secondNum-32-459.67)/1.8));
            case "N (Шкала Ньютона)":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum*100/33));
                else
                    return String.valueOf(df.format(secondNum*33/100));
            case "D (Шкала Дели́ля)":
                if (firstNumToSecond)
                    return String.valueOf(df.format((100-firstNum)*3/2));
                else
                    return String.valueOf(df.format(100-secondNum*2/3));
            default: return "Идентичные величины конвертации!";
        }
    }

    public static String weightConvert(String convertFrom, String convertTo, Double firstNum, Double secondNum, Boolean firstNumToSecond) {

        switch (convertTo) {
            case "g":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum * 1000));
                else
                    return String.valueOf(df.format(secondNum / 1000));
            case "c":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum * 100));
                else
                    return String.valueOf(df.format(secondNum / 100));
            case "carat":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum * 5000));
                else
                    return String.valueOf(df.format(secondNum / 5000));
            case "eng pound":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum * 2.20462));
                else
                    return String.valueOf(df.format(secondNum / 2.20462));
            case "pound":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / 0.37324));
                else
                    return String.valueOf(df.format(secondNum * 0.37324));
            case "stone":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / 6.35029));
                else
                    return String.valueOf(df.format(secondNum * 6.35029));
            case "rus pound":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / 2.441933));
                else
                    return String.valueOf(df.format(secondNum * 2.441933));
            default: return "Идентичные величины конвертации!";
        }
    }

    public static String timeConvert(String convertFrom, String convertTo, Double firstNum, Double secondNum, Boolean firstNumToSecond) {

        switch (convertTo) {
            case "Min":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / 60));
                else
                    return String.valueOf(df.format(secondNum * 60));
            case "Hour":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / (60*60)));
                else
                    return String.valueOf(df.format(secondNum * (60*60)));
            case "Day":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / (24*60*60)));
                else
                    return String.valueOf(df.format(secondNum * (24*60*60)));
            case "Week":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / (7*24*60*60)));
                else
                    return String.valueOf(df.format(secondNum * (7*24*60*60)));
            case "Month":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / (30*24*60*60)));
                else
                    return String.valueOf(df.format(secondNum * (30*24*60*60)));
            case "Astronomical Year":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / (365*24*60*60)));
                else
                    return String.valueOf(df.format(secondNum * (365*24*60*60)));
            case "Third":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum /(3*30*24*60*60)));
                else
                    return String.valueOf(df.format(secondNum * (3*30*24*60*60)));
            default: return "Идентичные величины конвертации!";
        }
    }

    public static String volumeConvert(String convertFrom, String convertTo, Double firstNum, Double secondNum, Boolean firstNumToSecond) {

        switch (convertTo) {
            case "m^3":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / 1000));
                else
                    return String.valueOf(df.format(secondNum * 1000));
            case "gallon":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / 3.78541));
                else
                    return String.valueOf(df.format(secondNum * 3.78541));
            case "pint":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum * 2.11338));
                else
                    return String.valueOf(df.format(secondNum / 2.11338));
            case "quart":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum * 1.05669));
                else
                    return String.valueOf(df.format(secondNum / 1.05669));
            case "barrel":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum * 0.006289810767583661));
                else
                    return String.valueOf(df.format(secondNum / 0.006289810767583661));
            case "cubic foot":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum / 28.3168));
                else
                    return String.valueOf(df.format(secondNum * 28.3168));
            case "cubic inch":
                if (firstNumToSecond)
                    return String.valueOf(df.format(firstNum * 61.0237));
                else
                    return String.valueOf(df.format(secondNum / 61.0237));
            default: return "Идентичные величины конвертации!";
        }
    }
}

